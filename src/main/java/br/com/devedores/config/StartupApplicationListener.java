package br.com.devedores.config;

import br.com.devedores.entities.Divida;
import br.com.devedores.entities.Usuario;
import br.com.devedores.services.DividaService;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    private DividaService dividaService;

    public StartupApplicationListener(DividaService dividaService) {
        this.dividaService = dividaService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        dividaService.save(
            Divida.builder()
                .usuario(Usuario.builder().id(Long.valueOf(1)).build())
                .motivo("Atraso na conta de água")
                .valor(BigDecimal.valueOf(1200))
                .data(LocalDateTime.now())
                .build()
        );
        dividaService.save(
            Divida.builder()
                .usuario(Usuario.builder().id(Long.valueOf(2)).build())
                .motivo("Parcelamentos de dívidas")
                .valor(BigDecimal.valueOf(900))
                .data(LocalDateTime.now())
                .build()
        );
        dividaService.save(
            Divida.builder()
                .usuario(Usuario.builder().id(Long.valueOf(3)).build())
                .motivo("Outras contas em atraso")
                .valor(BigDecimal.valueOf(12563.25))
                .data(LocalDateTime.now())
                .build()
        );
    }
}
