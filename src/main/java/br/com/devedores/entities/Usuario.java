package br.com.devedores.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;


@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Usuario {

    @Id
    private Long id;

    @NotBlank
    private String name;

    @Email
    private String email;

    @NotBlank
    private String phone;

}