package br.com.devedores.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table
@SequenceGenerator(name = "div_seq", sequenceName = "divida_seq", initialValue = 1, allocationSize = 1)
public class Divida {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "div_seq")
    private Long id;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    private Usuario usuario;

    @NotBlank
    private String motivo;

    @NotNull
    private LocalDateTime data;

    @NotNull
    private BigDecimal valor;

}
