package br.com.devedores.repositories;

import br.com.devedores.entities.Divida;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DividaRepository extends CrudRepository<Divida, Long> { }
