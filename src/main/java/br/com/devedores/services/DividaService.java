package br.com.devedores.services;

import br.com.devedores.entities.Divida;
import br.com.devedores.entities.Usuario;
import br.com.devedores.repositories.DividaRepository;
import javassist.NotFoundException;
import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class DividaService {

    private DividaRepository repository;

    private UsuarioService usuarioService;

    public DividaService(DividaRepository repository, UsuarioService usuarioService){
        this.repository = repository;
        this.usuarioService = usuarioService;
    }

    public Iterable<Divida> findAll() {
        return repository.findAll();
    }

    @Transactional
    public void save(Divida divida) {
        divida.setUsuario(usuarioService.findByIdAPI(divida.getUsuario().getId()));
        if ( usuarioService.existsUsuario(divida.getUsuario().getId()) )
            throw new ServiceException("Dívida já cadastrada para o Usuário informado");

        repository.save(divida);
    }

    public Optional<Divida> findById(Long id) {
        return repository.findById(id);
    }

    @Transactional
    public void update(Long id, Divida divida) throws NotFoundException {
        Divida saved = validate(id, divida.getUsuario().getId());
        divida.setId(saved.getId());
        divida.setUsuario(saved.getUsuario());
        repository.save(divida);
    }

    @Transactional
    public void delete(Long id) throws NotFoundException {
        Divida saved = validate(id, null);
        repository.deleteById(saved.getId());
    }

    private Divida validate(Long id, Long idUsuario) throws NotFoundException {
        Optional<Divida> saved = repository.findById(id);
        if (!saved.isPresent())
            throw new NotFoundException("Divida não cadastrada");
        if ( idUsuario != null && !idUsuario.equals(saved.get().getUsuario().getId()) )
            throw new ServiceException("Divida não pode ser trocada para outro usuário");

        return saved.get();
    }
}
