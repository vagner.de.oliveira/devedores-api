package br.com.devedores.services;

import br.com.devedores.entities.Usuario;
import br.com.devedores.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class UsuarioService {

    @Value("${jsonplaceholder.url}")
    private String uri;

    private UsuarioRepository repository;

    public UsuarioService(UsuarioRepository repository) {
        this.repository = repository;
    }

    public Iterable<Usuario> findAllAPI() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Iterable<Usuario>> response = restTemplate.exchange(
            uri,
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<Iterable<Usuario>>(){}
        );
        return response.getBody();
    }

    public Usuario findByIdAPI(Long id) {
        StringBuilder path = new StringBuilder(uri).append("/").append(id);
        return new RestTemplate().getForObject(path.toString(), Usuario.class);
    }

    public boolean existsUsuario(Long id) {
        return repository.existsById(id);
    }

    public Optional<Usuario> findById(Long idUsuario) {
        return repository.findById(idUsuario);
    }
}
