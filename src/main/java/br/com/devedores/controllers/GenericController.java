package br.com.devedores.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

public class GenericController {

    private HttpHeaders getHeaders(String path, String identifier, Object o){
        final URI location = ServletUriComponentsBuilder.fromCurrentServletMapping()
                .path(path + identifier)
                .build()
                .expand(o)
                .toUri();

        final HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return headers;
    }

    protected ResponseEntity<Void> responseEntityCreated(String path, Long id) {
        return new ResponseEntity<Void>(
            getHeaders(path, "/{id}", id),
            HttpStatus.CREATED
        );
    }
}
