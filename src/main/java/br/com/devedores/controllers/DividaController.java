package br.com.devedores.controllers;

import br.com.devedores.entities.Divida;
import br.com.devedores.entities.Usuario;
import br.com.devedores.services.DividaService;
import br.com.devedores.services.UsuarioService;
import javassist.NotFoundException;
import org.hibernate.service.spi.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/dividas")
public class DividaController extends GenericController {

    public static final String URL_DIVIDAS = "/dividas";

    private DividaService service;

    private UsuarioService usuarioService;

    public DividaController(DividaService service, UsuarioService usuarioService){
        this.service = service;
        this.usuarioService = usuarioService;
    }

    @GetMapping
    public Iterable<Divida> list(){
        return service.findAll();
    }

    @GetMapping("/usuarios")
    public Iterable<Usuario> usuarios(){
        return usuarioService.findAllAPI();
    }

    @PostMapping
    public ResponseEntity save(@Valid @RequestBody Divida divida){
        try {
            service.save(divida);
            return responseEntityCreated(URL_DIVIDAS, divida.getId());
        } catch (Exception e){
            return new ResponseEntity(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity find(@PathVariable("id") Long id){
        Optional<Divida> divida = service.findById(id);
        if ( divida.isPresent() )
            return ResponseEntity.ok(divida.get());

        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@Valid @RequestBody Divida divida, @PathVariable("id") Long id) throws NotFoundException {
        try {
            service.update(id, divida);
            return ResponseEntity.noContent().build();
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (ServiceException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id){
        try {
            service.delete(id);
            return ResponseEntity.noContent().build();
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
